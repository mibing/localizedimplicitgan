import math

import torch
import torch.distributions
import torch.nn as nn
import torch.nn.functional as fn

import layer
import util


class AutoEncoder(nn.Module):
    def __init__(self, bottleneck, noise, interpolate, vae, grid):
        super().__init__()

        self.grid = grid
        self.bottleneck = bottleneck
        self.noise = noise
        self.interpolate = interpolate
        self.vae = vae
        if vae:
            self.bottleneck *= 2

    def encode(self, voxel):
        if self.grid:
            b = voxel.shape[0]
            p = self.patch_size // self.patch_padding
            # voxel = util.split_grid(voxel, 32, 14) # specific for architecture
            voxel = util.split_grid(voxel, self.patch_size, self.patch_padding)  # specific for architecture
            n_grid = voxel.shape[0]
            step = n_grid // b

            z = []
            for i in range(step):
                inp = voxel[i * b: (i + 1) * b]
                z.append(self.encoder(inp.unsqueeze(1).float()))
            z = torch.cat(z, dim=0)
            z = z.view(b, 8, 8, 8, self.bottleneck, p, p, p).permute(0, 4, 1, 5, 2, 6, 3, 7)
            z = z.reshape(b, self.bottleneck, 8 * p, 8 * p, 8 * p)
        else:
            z = self.encoder(voxel.unsqueeze(1))

        if self.noise > 0:
            z = z + torch.randn_like(z) * self.noise

        if self.vae:
            if self.training:
                m, s = z[:, :self.bottleneck // 2], z[:, (self.bottleneck // 2):]
                dist = torch.distributions.Normal(m, s.exp())
                z = dist.rsample()

                return z, dist
            else:
                z = z[:, :self.bottleneck // 2]

        return z, None

    def decode(self, z, samples):
        sample_limit = 100000
        step = math.ceil(samples.shape[-1] / sample_limit)
        if samples.shape[-1] > sample_limit:
            out = []
            for i in range(step):
                batch_sample = samples[:, :, i * sample_limit: (i + 1) * sample_limit]
                out.append(self.classifier(z, batch_sample).squeeze(1))
            out = torch.cat(out, dim=1)
        else:
            out = self.classifier(z, samples).squeeze(1)
        return out

    def forward(self, voxel, samples):
        z, dist = self.encode(voxel)
        out = self.decode(z, samples)

        if self.vae:
            return out, z, dist
        else:
            return out, z


class VoxelEncoding(AutoEncoder):
    def __init__(self, bottleneck=32, noise=0, interpolate=False, vae=False, grid=False):
        super().__init__(bottleneck, noise, interpolate, vae, grid)

        self.encoder = nn.Sequential(
            nn.Conv3d(1, 32, 3, 1, 1, bias=False),
            nn.BatchNorm3d(32),
            nn.LeakyReLU(),
            nn.Conv3d(32, 64, 3, 1, 1, bias=False),
            nn.BatchNorm3d(64),
            nn.LeakyReLU(),
            nn.Conv3d(64, 64, 4, 2, 1, bias=False),
            nn.BatchNorm3d(64),
            nn.LeakyReLU(),
            nn.Conv3d(64, 64, 1, 1, 0, bias=False),
            nn.BatchNorm3d(64),
            nn.LeakyReLU(),
            nn.Conv3d(64, self.bottleneck, 1, 1, 0, bias=True)
        )

        if interpolate:
            self.classifier = layer.ImplicitCheckInterpolate(
                nn.Sequential(nn.Conv1d(bottleneck + 3, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 32, 1, bias=False),
                              nn.BatchNorm1d(32),
                              nn.LeakyReLU(),
                              nn.Conv1d(32, 32, 1, bias=False),
                              nn.BatchNorm1d(32),
                              nn.LeakyReLU(),
                              nn.Conv1d(32, 1, 1),
                              nn.Sigmoid()))
        else:
            self.classifier = layer.ImplicitCheck(
                nn.Sequential(nn.Conv1d(bottleneck + 3, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 32, 1, bias=False),
                              nn.BatchNorm1d(32),
                              nn.LeakyReLU(),
                              nn.Conv1d(32, 32, 1, bias=False),
                              nn.BatchNorm1d(32),
                              nn.LeakyReLU(),
                              nn.Conv1d(32, 1, 1),
                              nn.Sigmoid()))

    def encode(self, voxel):
        z = self.encoder(voxel.unsqueeze(1))

        if self.noise > 0:
            z = z + torch.randn_like(z) * self.noise
        return z, None


class VoxelEncodingHighRes(AutoEncoder):
    def __init__(self, bottleneck=32, noise=0, interpolate=False, vae=False, grid=False):
        super().__init__(bottleneck, noise, interpolate, vae, grid)

        self.patch_size = 32
        self.patch_padding = 8

        self.encoder = nn.Sequential(
            nn.Conv3d(1, 32, 3, bias=False),
            nn.BatchNorm3d(32),
            nn.LeakyReLU(),
            nn.Conv3d(32, 32, 4, 2, bias=False),
            nn.BatchNorm3d(32),
            nn.LeakyReLU(),
            nn.Conv3d(32, 64, 1, bias=False),
            nn.BatchNorm3d(64),
            nn.LeakyReLU(),
            nn.Conv3d(64, 64, 4, 2, bias=False),
            nn.BatchNorm3d(64),
            nn.LeakyReLU(),
            nn.Conv3d(64, 128, 1, bias=False),
            nn.BatchNorm3d(128),
            nn.LeakyReLU(),
            nn.Conv3d(128, 128, 4, 2, bias=False),
            nn.BatchNorm3d(128),
            nn.LeakyReLU(),
            nn.Conv3d(128, self.bottleneck, 1, bias=True),
        )

        if interpolate:
            self.classifier = layer.ImplicitCheckInterpolate(
                nn.Sequential(nn.Conv1d(bottleneck + 3, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 32, 1, bias=False),
                              nn.BatchNorm1d(32),
                              nn.LeakyReLU(),
                              nn.Conv1d(32, 32, 1, bias=False),
                              nn.BatchNorm1d(32),
                              nn.LeakyReLU(),
                              nn.Conv1d(32, 1, 1),
                              nn.Sigmoid()))
        else:
            self.classifier = layer.ImplicitCheck(
                nn.Sequential(nn.Conv1d(bottleneck + 3, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 64, 1, bias=False),
                              nn.BatchNorm1d(64),
                              nn.LeakyReLU(),
                              nn.Conv1d(64, 32, 1, bias=False),
                              nn.BatchNorm1d(32),
                              nn.LeakyReLU(),
                              nn.Conv1d(32, 32, 1, bias=False),
                              nn.BatchNorm1d(32),
                              nn.LeakyReLU(),
                              nn.Conv1d(32, 1, 1),
                              nn.Sigmoid()))


def load_auto_encoder(arch, latent_dim, path=None, interpolate=False, vae=False, noise=0, grid=False):
    if arch == 'low-res':
        model = VoxelEncoding(latent_dim, noise, interpolate, grid=grid)
    elif arch == 'high-res':
        model = VoxelEncodingHighRes(latent_dim, noise, interpolate, vae, grid=grid)
    else:
        print("ERROR: invalid AE architecture")
        return

    if path is not None:
        model.load_state_dict(torch.load(path))

    return model


class DiscriminatorPatchResidual(nn.Module):
    def __init__(self, h_dim=32, mask_layer=2, resolution=32):
        super().__init__()
        self.resolution = resolution

        self.main = nn.Sequential(
            layer.ResidualBlock(h_dim + mask_layer, 32, 3, 1),
            layer.ResidualBlock(32, 64, 4, 2),
            layer.ResidualBlock(64, 128, 4, 2),
            torch.nn.utils.spectral_norm(nn.Conv3d(128, 1, 1, 1, 0), n_power_iterations=5)
        )

    def forward(self, x, mask=None):
        x = fn.interpolate(x, size=self.resolution, mode='trilinear', align_corners=False)
        if mask is not None:
            mask = fn.interpolate(mask.float(), size=self.resolution, mode='trilinear', align_corners=False)
            x = torch.cat([x, mask], dim=1)
        output = self.main(x).squeeze()
        return output


class DiscriminatorStandard(nn.Module):
    def __init__(self, h_dim=32, mask_layer=2):
        super().__init__()

        self.critic1 = DiscriminatorPatchResidual(h_dim, mask_layer, 32)
        self.critic2 = DiscriminatorPatchResidual(h_dim, mask_layer, 16)
        self.critic3 = DiscriminatorPatchResidual(h_dim, mask_layer, 8)

    def forward(self, x, mask):
        out1 = self.critic1(x, mask)
        out2 = self.critic2(x, mask)
        out3 = self.critic3(x, mask)

        return [out1, out2, out3]


class GeneratorConvolutional(nn.Module):
    def __init__(self, h_dim=32, noise_dim=128):
        super().__init__()

        self.decoder = nn.Sequential(
            nn.ConvTranspose3d(noise_dim, 128, 4, 1, 0, bias=False),
            nn.BatchNorm3d(128),
            nn.LeakyReLU(),
            nn.Conv3d(128, 128, 3, 1, 1, bias=False),
            nn.BatchNorm3d(128),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(128, 128, 4, 2, 1, bias=False),
            nn.BatchNorm3d(128),
            nn.LeakyReLU(),
            nn.Conv3d(128, 64, 3, 1, 1, bias=False),
            nn.BatchNorm3d(64),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(64, 64, 4, 2, 1, bias=False),
            nn.BatchNorm3d(64),
            nn.LeakyReLU(),
            nn.Conv3d(64, 32, 3, 1, 1, bias=False),
            nn.BatchNorm3d(32),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(32, 32, 4, 2, 1, bias=False),
            nn.BatchNorm3d(32),
            nn.LeakyReLU(),
            nn.Conv3d(32, h_dim, 3, 1, 1, bias=True)
        )

    def forward(self, z, mask=None):
        return self.decoder(z[:, :, None, None, None])


class GeneratorSPADEResSkip(nn.Module):
    def __init__(self, h_dim=32, noise_dim=128, mask_layer=2, max_pool=False):
        super().__init__()

        self.noise_dim = noise_dim

        self.pre = nn.Linear(noise_dim, 128 * 8)

        self.decoder = layer.ConditionalDecoder(nn.ModuleList([
            layer.SPADEResidualBlock(128, 128, mask_layer, max_pool),
            nn.Upsample(scale_factor=2, mode='trilinear', align_corners=False),  # 4
            layer.SPADEResidualBlock(128, 128, mask_layer, max_pool),
            nn.Upsample(scale_factor=2, mode='trilinear', align_corners=False),  # 8
            layer.SPADEResidualBlock(128, 64, mask_layer, max_pool),
            nn.Upsample(scale_factor=2, mode='trilinear', align_corners=False),  # 16
            layer.SPADEResidualBlock(64, 64, mask_layer, max_pool),
            nn.Upsample(scale_factor=2, mode='trilinear', align_corners=False),  # 32
            layer.SPADEResidualBlock(64, h_dim, mask_layer, max_pool)
        ]), noise=False, skip=True, out_shape=(h_dim, 32, 32, 32,))

        self.post = nn.Sequential(
            nn.Conv3d(h_dim, h_dim, 3, padding=1, bias=False),
            nn.BatchNorm3d(h_dim),
            nn.LeakyReLU(),
            nn.Conv3d(h_dim, h_dim, 3, padding=1, bias=False),
            nn.BatchNorm3d(h_dim),
            nn.LeakyReLU(),
            nn.Conv3d(h_dim, h_dim, 3, padding=1, bias=True)
        )

    def forward(self, z, mask=None):
        z = self.pre(z)
        z = z.view(-1, 128, 2, 2, 2)
        x = self.post(self.decoder(z, mask))
        return x


def load_generator(arch, latent_dim, noise_dim=128, mask_layer=0, path=None, parallel=False):
    if arch == 'convolutional':
        model = GeneratorConvolutional(latent_dim, noise_dim)
    elif arch == 'conditional':
        model = GeneratorSPADEResSkip(latent_dim, noise_dim, mask_layer=mask_layer, max_pool=False)
    else:
        print("ERROR: invalid generator architecture")
        return

    if parallel:
        model = nn.DataParallel(model)

    if path is not None:
        model.load_state_dict(torch.load(path))

    return model