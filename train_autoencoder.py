import argparse
import os
import pickle

import torch
import torch.utils.data

import datasets
import losses
import models
import trainer
import util


def get_dataloader(opt):
    assert opt.dataset in ["high-res", "low-res", "spheres", "precomputed"]

    if opt.dataset == 'high-res':
        train_data = datasets.ShapeNetIMGAN(opt.data_path,
                                            opt.class_label,
                                            mode='train',
                                            transform=None,
                                            resolution_input=32,
                                            n_samples_boundary=5000,
                                            n_samples_rand=1000,
                                            subdivide=True,
                                            padding=8)

        train_dataloader = torch.utils.data.DataLoader(train_data,
                                                       batch_size=opt.batch_size,
                                                       shuffle=True,
                                                       drop_last=False)

    elif opt.dataset == 'low-res':
        train_data = datasets.ShapeNetIMGAN(opt.data_path,
                                            opt.class_label,
                                            mode='train',
                                            transform=None,
                                            resolution_input=64,
                                            n_samples_boundary=5000,
                                            n_samples_rand=1000,
                                            subdivide=False)

        train_dataloader = torch.utils.data.DataLoader(train_data,
                                                       batch_size=opt.batch_size,
                                                       shuffle=True,
                                                       drop_last=False,
                                                       collate_fn=train_data.sample_objects)

    else:
        train_dataloader = None

    return train_dataloader


def train(opt):
    util.save_params(opt)
    train_dataloader = get_dataloader(opt)

    if opt.kld_loss > 0:
        vae = True
    else:
        vae = False

    loss_function = losses.BinaryLoss(opt)

    net = models.load_auto_encoder(opt.arch, opt.bottleneck, None,
                                   opt.interpolate, vae, opt.noise).to(opt.device)

    optimizer = torch.optim.Adam(net.parameters(), lr=opt.lr, amsgrad=True, weight_decay=opt.weight_decay)

    logger = trainer.Logger(opt.run_name, log_tqdm=False, log_tensorboard=True)
    network_trainer = trainer.Trainer(net, loss_function, optimizer, train_dataloader,
                                      logger=logger)
    if opt.restore:
        network_trainer.restore(os.path.join("models", opt.run_name), "train", "complete")
    elif opt.restore_dir:
        network_trainer.restore(opt.restore_dir, "train", "model_only")
    network_trainer.train(n_epochs=opt.n_epochs)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--run-name', default='tmp')
    parser.add_argument('--data-path', default='tmp')
    parser.add_argument('--dataset', default='high-res')
    parser.add_argument('--class-label', default='03001627')
    parser.add_argument('--n-epochs', type=int, default=1000)
    parser.add_argument('--batch-size', type=int, default=32)
    parser.add_argument('--lr', type=float, default=1e-3)
    parser.add_argument('--restore', action='store_true')
    parser.add_argument('--restore-dir', default='')
    parser.add_argument('--random-seed', type=int, default=42)
    parser.add_argument('--no-cuda', action='store_true')
    # losses + reguarization
    parser.add_argument('--kld-loss', type=float, default=0)
    parser.add_argument('--regularization', type=float, default=0)
    parser.add_argument('--noise', type=float, default=0)
    parser.add_argument('--weight-decay', type=float, default=0)
    # ae parameter
    parser.add_argument('--arch', default='high-res')
    parser.add_argument('--bottleneck', type=int, default=32)
    parser.add_argument('--latent-size', type=int, default=128)
    parser.add_argument('--interpolate', action='store_true')

    args = parser.parse_args()

    if args.restore:
        args = pickle.load(open(os.path.join("models", args.run_name, 'opt.pkl'), "rb"))
        args.restore = True
    util.set_random_seed(args.random_seed, False)
    args.device = torch.device('cpu') if args.no_cuda else torch.device('cuda')

    print(args)
    train(args)
