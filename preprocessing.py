import math

import numpy as np
import torch
import torch.nn.functional as fn
from scipy.io import loadmat


def silhouette_hsp(file_path, resolution, axis):
    if axis == 0:
        id_a, id_b = 1, 2
    elif axis == 1:
        id_a, id_b = 0, 2
    elif axis == 2:
        id_a, id_b = 0, 1

    file = loadmat(file_path)

    boundary = (file["bi"] > 1).nonzero()
    boundary_cells = file["b"][file["bi"][boundary[0], boundary[1], boundary[2]].astype(int) - 1]
    img = torch.zeros(256, 256, dtype=int)
    for i in range(boundary[0].shape[0]):
        img[boundary[id_a][i] * 16: (boundary[id_a][i] + 1) * 16,
        boundary[id_b][i] * 16: (boundary[id_b][i] + 1) * 16] = \
            torch.max(img[boundary[id_a][i] * 16: (boundary[id_a][i] + 1) * 16,
                      boundary[id_b][i] * 16: (boundary[id_b][i] + 1) * 16],
                      torch.from_numpy(boundary_cells[i]).max(dim=axis)[0].long())

    img = fn.adaptive_max_pool2d(img[None, None].float(), resolution).squeeze().long()
    if axis == 0:
        return img.unsqueeze(axis).repeat([resolution, 1, 1])
    elif axis == 1:
        return img.unsqueeze(axis).repeat([1, resolution, 1])
    elif axis == 2:
        return img.unsqueeze(axis).repeat([1, 1, resolution])

    return img


def hull_hsp(file_path, resolution):
    img0 = silhouette_hsp(file_path, resolution, 0)
    img1 = silhouette_hsp(file_path, resolution, 1)
    img2 = silhouette_hsp(file_path, resolution, 2)
    return torch.min(img0, torch.min(img1, img2))


def load_hsp(file_path, resolution=None, n_samples_rand=0, n_samples_boundary=0):
    file = loadmat(file_path)

    boundary = (file["bi"] > 2).nonzero()
    boundary = np.stack([boundary[0], boundary[1], boundary[2]])

    if resolution is not None:
        grid = torch.zeros((resolution, resolution, resolution))
        step = 256 // resolution
        reach = 16 // step

        full = np.stack((file["bi"] == 2.0).nonzero()) * reach
        n_full = full.shape[-1]

        if n_full > 0:
            full = np.repeat(np.expand_dims(full, 1), reach, 1)
            full = np.add(full, np.array(range(reach))[None, :, None])
            full = np.stack([np.array(np.meshgrid(full[0, :, i],
                                                  full[1, :, i],
                                                  full[2, :, i])) for i in range(n_full)], axis=4).reshape(3, -1)

            grid[full[0], full[1], full[2]] = 1

        boundary_cells = file["b"][file["bi"][boundary[0], boundary[1], boundary[2]].astype(int) - 1]

        for l in range(reach):
            for m in range(reach):
                for n in range(reach):
                    cell = boundary_cells[:,
                           l * step:(l + 1) * step,
                           m * step:(m + 1) * step,
                           n * step:(n + 1) * step].max(axis=-1).max(axis=-1).max(axis=-1)
                    grid[boundary[0] * reach + l,
                         boundary[1] * reach + m,
                         boundary[2] * reach + n] = torch.from_numpy(cell).float()
    else:
        grid = torch.empty(1)

    if n_samples_boundary + n_samples_rand > 0:
        random_samples = torch.randint(16, (3, n_samples_rand))
        if boundary.shape[1] > 0:
            boundary = torch.from_numpy(boundary[:, np.random.choice(boundary.shape[1], n_samples_boundary)])
        else:
            boundary = torch.randint(16, (3, n_samples_boundary))

        outer = torch.cat([boundary, random_samples], dim=1)
        inner = torch.randint(16, (3, n_samples_boundary + n_samples_rand))

        if resolution is None:
            first = file["bi"][outer[0], outer[1], outer[2]].astype(int)
            score = torch.from_numpy(file["b"][first - 1, inner[0], inner[1], inner[2]]).float()
            position = (outer.float() / 16.0 + inner.float() / 256.0) - 0.5 + (1 / 512)
        else:
            index = ((outer * 16 + inner) / (256 / resolution)).floor().long()
            score = grid[index[0], index[1], index[2]]
            position = (outer.float() / 16.0 + inner.float() / 256.0) - 0.5 + (1 / 512)
    else:
        position, score = torch.empty(1), torch.empty(1)

    return grid, position, score


# size 32, padding 8
def sample_hsp(file_path, boundary_factor=0.9, cell_size=32, padding=0, n_cells=8, n_samples=5000, rigid=False):
    file = loadmat(file_path)

    n_boundary = (torch.rand(n_cells) > (1 - boundary_factor)).sum().item()
    n_random = n_cells - n_boundary
    resolution = cell_size + padding * 2
    outer_padding = math.ceil(resolution / 32.0)

    boundary = (file["bi"] > 2).nonzero()
    boundary = np.stack([boundary[0], boundary[1], boundary[2]])

    random_samples = torch.randint(16, (3, n_random))
    if boundary.shape[-1] > 0:
        boundary = torch.from_numpy(boundary[:, np.random.choice(boundary.shape[1], n_boundary)])
    else:
        boundary = torch.randint(16, (3, n_boundary))

    outer_index = torch.cat([boundary, random_samples], dim=1)  # index of outer cells
    if rigid:
        inner_index = torch.zeros((3, n_cells)).int()
        outer_index = outer_index // 2 * 2 + 1  # TODO this is specific to cell_size=32
    else:
        inner_index = torch.randint(16, (3, n_cells))  # index of inner cells

    index_256 = outer_index * 16 + inner_index - (cell_size // 2)  # starting index
    index_32 = index_256 // 8

    outer_index = outer_index + outer_padding
    inner_index = inner_index + (outer_padding * 16)

    outer = np.pad(file["bi"], (outer_padding, outer_padding), 'constant', constant_values=(1, 1))

    outer_index = np.repeat(np.expand_dims(outer_index, 1), 2 * outer_padding + 1, 1)
    outer_index = np.add(outer_index, (np.array(range(2 * outer_padding + 1)) - outer_padding)[None, :, None])
    outer_index = np.stack([np.array(np.meshgrid(outer_index[0, :, i],
                                                 outer_index[1, :, i],
                                                 outer_index[2, :, i], indexing='ij')) for i in range(n_cells)], axis=4)

    outer_cells = outer[outer_index[0], outer_index[1], outer_index[2]].astype(int) - 1

    inner_size = (2 * outer_padding + 1) * 16
    inner = file["b"][outer_cells]

    inner = inner.transpose(0, 4, 1, 5, 2, 6, 3).reshape(inner_size, inner_size, inner_size, -1)

    inner_index = np.repeat(np.expand_dims(inner_index, 1), resolution, 1)
    inner_index = np.add(inner_index, (np.array(range(resolution)) - (resolution // 2))[None, :, None])
    inner_index = np.stack([np.array(np.meshgrid(inner_index[0, :, i],
                                                 inner_index[1, :, i],
                                                 inner_index[2, :, i], indexing='ij')) for i in range(n_cells)], axis=4)

    out = [inner[inner_index[0, :, :, :, i], inner_index[1, :, :, :, i], inner_index[2, :, :, :, i], i] for i in
           range(n_cells)]
    out = np.stack(out, axis=0).astype(np.single)

    if n_random > 0:
        n_random_samples = n_samples
        n_boundary_samples = 0
    else:
        n_boundary_samples = int(n_samples * boundary_factor)
        n_random_samples = n_samples - n_boundary_samples

    samples = torch.randint(cell_size, (n_cells, 3, n_random_samples))
    positions = (samples + 0.5) / cell_size

    # positions = corner.unsqueeze(1).repeat([1,n_samples,1]) + samples.float() / 256.0
    samples += padding
    scores = np.stack([out[i, samples[i, 0, :], samples[i, 1, :], samples[i, 2, :]] for i in range(n_cells)], axis=0)
    scores = torch.from_numpy(scores)

    if n_boundary_samples > 0:
        boundary_positions, boundary_scores = boundary_sampling(torch.from_numpy(out), padding, n_boundary_samples)
        positions = torch.cat([positions, boundary_positions], dim=-1)
        scores = torch.cat([scores, boundary_scores], dim=-1)

    positions -= 0.5
    return torch.from_numpy(out), positions, scores, index_32


def boundary_sampling(patches, padding, n_samples):
    n_cells = patches.shape[0]
    filter = torch.ones((3, 3, 3)) * (-1. / 26.)
    filter[1, 1, 1] = 1.0

    offset = padding - 1
    if offset >= 0:
        boundary = patches[:, offset:-offset, offset:-offset, offset:-offset].float()
    else:
        boundary = fn.pad(patches, [1, 1, 1, 1, 1, 1], "constant", 0)
    boundary = fn.conv3d(boundary[:, None, :, :, :], filter[None, None, :, :, :]).squeeze(1)
    cell_size = boundary.shape[-1]

    indices = torch.empty((n_cells, 3, n_samples), dtype=torch.long)
    for i in range(n_cells):
        index = boundary[i].nonzero(as_tuple=False)
        if index.shape[0] > 0:
            samples = torch.randint(index.shape[0], (n_samples,))
            indices[i] = index[samples, :].transpose(1, 0).long()
        else:
            size = boundary.shape[-1]
            indices[i] = torch.randint(size, (3, n_samples))

    positions = (indices + 0.5) / cell_size
    indices += padding
    scores = torch.stack([patches[i, indices[i, 0, :], indices[i, 1, :], indices[i, 2, :]] for i in range(n_cells)],
                         dim=0)

    return positions, scores
