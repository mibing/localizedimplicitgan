import numpy as np
import torch
import torch.distributions
import torch.nn.functional as fn

import trainer


class GeneratorLoss(trainer.LossFunctionGenerator):
    def __init__(self, options=None):
        super().__init__(options=options)

    def __call__(self, critic, real_data, fake_data, add_data):
        loss_dict = dict()
        total_loss = 0
        output = critic(fake_data, add_data)
        for i in range(len(output)):
            if self.options.loss == "wgan":
                loss = trainer.wgan_loss_generator(output[i])
            elif self.options.loss == "hinge":
                loss = trainer.hinge_loss_generator(output[i])
            elif self.options.loss == "NS":
                loss = trainer.ns_loss_generator(output[i], reduce='mean')
            else:
                loss = trainer.gan_loss_generator(output[i], reduce='mean')

            out = torch.sigmoid(output[i].detach()).mean().item()
            loss_dict['loss_g' + str(i)] = loss
            loss_dict['output_fake2' + str(i)] = out
            total_loss += loss

        return loss_dict, total_loss


class CriticLossPatch(trainer.LossFunctionCritic):
    def __init__(self, options=None):
        super().__init__(options=options)

    def __call__(self, critic, real_data, fake_data, add_data):

        if self.options.gradient_penalty > 0 and self.options.penalty_target == "data":
            real_data.requires_grad = True
            fake_data.requires_grad = True

        output_fake = critic(fake_data, add_data)
        output_real = critic(real_data, add_data)

        loss = 0
        loss_dict = {}

        for i in range(len(output_fake)):
            gradient_penalty = self.options.gradient_penalty / ((output_fake[i].shape[-1]) ** 3)

            if self.options.gradient_penalty > 0 and self.options.penalty_target == "data":
                grad_reg = trainer.gradient_regularization(fake_data, output_fake[i], self.options.penalty_center,
                                                               self.options.penalty_reduction) * gradient_penalty
                grad_reg.backward(retain_graph=True)
                grad_norm = grad_reg.item()

                grad_reg = trainer.gradient_regularization(real_data, output_real[i], self.options.penalty_center,
                                                               self.options.penalty_reduction) * gradient_penalty
                grad_reg.backward(retain_graph=True)
                grad_norm += grad_reg.item()
            elif self.options.gradient_penalty > 0 and self.options.penalty_target == "interpolation":
                alpha = torch.rand(fake_data.shape[0], 1, 1, 1, 1, device=self.options.device).expand_as(fake_data)
                median_data = (alpha * fake_data.data +
                               (torch.ones_like(alpha) - alpha) * real_data.data).requires_grad_(True)
                output_median = getattr(critic, 'critic' + str(i + 1))(median_data, add_data)

                grad_reg = trainer.gradient_regularization(median_data, output_median,
                                                               self.options.penalty_center,
                                                               self.options.penalty_reduction) * gradient_penalty
                grad_norm = grad_reg.item()
                grad_reg.backward()
            else:
                grad_norm = 0

            if self.options.loss == "wgan":
                loss_d = trainer.wgan_loss_critic(output_real[i], output_fake[i])
            elif self.options.loss == "hinge":
                loss_d = trainer.hinge_loss_critic(output_real[i], output_fake[i])
            elif self.options.loss == "NS":
                loss_d = trainer.ns_loss_critic(output_real[i], output_fake[i], self.options.soft_labels,
                                                    self.options.noisy_labels, reduce='mean')
            else:
                loss_d = trainer.gan_loss_critic(output_real[i], output_fake[i], self.options.soft_labels,
                                                     self.options.noisy_labels, reduce='mean')

            out_fake = torch.sigmoid(output_fake[i].detach()).mean().item()
            out_real = torch.sigmoid(output_real[i].detach()).mean().item()

            loss += loss_d

            loss_dict['loss_d' + str(i)] = loss_d
            loss_dict['output_fake' + str(i)] = out_fake
            loss_dict['output_real' + str(i)] = out_real
            loss_dict['grad_norm' + str(i)] = grad_norm

        return loss_dict, loss


class BinaryLoss(trainer.LossFunction):
    def __init__(self, options=None):
        super().__init__(options=options)

    def __call__(self, model, batch_data):
        samples = batch_data[0].to(self.options.device)
        target = batch_data[1].to(self.options.device)
        grid = batch_data[2].to(self.options.device)

        out = model(grid, samples)
        pred = out[0]

        if self.options.interpolate and model.training:
            target = target[:, None, None, None, :].repeat([1, 3, 3, 3, 1])

        loss_dict = {'acc': (pred.round() == target).sum().float() / np.prod(target.shape)}

        weights = target.mean()  # TODO what effect does this have?
        weights = torch.stack([weights, 1 - weights])
        weights = weights[target.long()]

        rec_loss = fn.binary_cross_entropy(pred, target, weights, reduction='mean')
        total_loss = rec_loss
        loss_dict['reconstruction'] = rec_loss

        if self.options.regularization > 0:
            latent = out[1]
            reg_loss = torch.norm(latent, p=2, dim=1).mean() * self.options.regularization
            total_loss = total_loss + reg_loss
            loss_dict['regularization'] = reg_loss

        if self.options.kld_loss > 0:
            dist = out[2]
            p = torch.distributions.Normal(torch.zeros_like(dist.mean), torch.ones_like(dist.scale))
            kld_loss = torch.distributions.kl.kl_divergence(dist, p).mean() * self.options.kld_loss
            total_loss = total_loss + kld_loss
            loss_dict['kld'] = kld_loss

        loss_dict['total'] = total_loss
        return loss_dict
