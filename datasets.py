from glob import glob

import h5py
import numpy as np
import torch.nn.functional as fn
import torch.utils.data
import trimesh

import preprocessing
import trainer
import util


class ShapeNetIMGAN(torch.utils.data.Dataset):
    def __init__(self, data_path, class_id=None, mode='train', transform=None,
                 resolution_input=None, n_samples_boundary=0, n_samples_rand=0, subdivide=False, rigid=False,
                 padding=8, representation='voxel'):
        super().__init__()

        assert mode in ['train', 'test'], "mode '%s' is not supported" % mode

        self.representation = representation
        self.subdivide = subdivide
        self.transform = transform
        self.resolution_input = resolution_input
        self.n_samples_boundary = n_samples_boundary
        self.n_samples_rand = n_samples_rand
        self.rigid = rigid
        self.padding = padding

        if class_id == 'complete':
            self.filenames = []
            directories = sorted(glob(data_path + "/*"))
            for i, path in enumerate(directories):
                mesh_files = sorted(glob(path + "/*.mat"))
                n_files = len(mesh_files)
                if mode == 'train':
                    self.filenames = self.filenames + mesh_files[:int(0.8 * n_files)]
                else:
                    self.filenames = self.filenames + mesh_files[int(0.8 * n_files):]
        else:
            mesh_files = sorted(glob(data_path + "/" + str(class_id) + "/*"))

            if mode == 'train':
                self.filenames = mesh_files[: int(0.8 * len(mesh_files))]
            else:
                self.filenames = mesh_files[int(0.8 * len(mesh_files)):]

    def __getitem__(self, index):
        path = self.filenames[index]
        if self.representation == 'mesh':
            scene = trimesh.load(path + "/model.obj", process=False)
            if isinstance(scene, trimesh.Scene):
                mesh = trimesh.util.concatenate(
                    tuple(trimesh.Trimesh(vertices=g.vertices, faces=g.faces) for g in scene.geometry.values()))
            else:
                mesh = scene

            return mesh
        else:
            if self.subdivide:
                factor = self.n_samples_boundary / (self.n_samples_boundary + self.n_samples_rand)
                n_samples = (self.n_samples_boundary + self.n_samples_rand)

                # padding = 8  # fixed value for given architecture HighRes
                # padding = 0  # fixed value for given architecture HighResLocal

                grid, position, score, index = preprocessing.sample_hsp(path, factor, self.resolution_input,
                                                                        self.padding,
                                                                        1, n_samples, self.rigid)
                grid, position, score, index = grid.squeeze(0), position.squeeze(0), score.squeeze(0), index.squeeze(0)
                position = util.jitter(position, self.resolution_input)
                # position = util.jitter_sphere(position, 64).clamp(0, 1.0)
                return position, score, grid, path, index
            else:
                return path

    def __len__(self):
        return len(self.filenames)

    def sample_objects(self, file_paths):
        positions = []
        scores = []
        grids = []
        for path in file_paths:
            grid, position, score = preprocessing.load_hsp(path, self.resolution_input,
                                                           self.n_samples_rand, self.n_samples_boundary)
            position = util.jitter(position, 256)
            # position = util.jitter_sphere(position, 256)
            positions.append(position)
            scores.append(score.float())
            grids.append(grid)

        positions = torch.stack(positions)
        scores = torch.stack(scores)
        grids = torch.stack(grids)

        return positions, scores, grids, file_paths


class ShapeNetPrecomputed(torch.utils.data.Dataset):
    def __init__(self, latent_path, voxel_path=None, bb_path=None, part_bb_path=None, part_path=None,
                 neg_bb=False, hull=False, silhouette=False):
        super().__init__()

        self.latent_path = latent_path
        self.part_path = part_path
        self.bb_path = bb_path
        self.part_bb_path = part_bb_path
        self.voxel_path = voxel_path
        self.neg_bb = neg_bb
        self.hull = hull
        self.silhouette = silhouette

    def __getitem__(self, index):
        if self.bb_path is not None:
            with h5py.File(self.bb_path, "r") as f:
                bb = torch.from_numpy(f['aabb'][index]).float()
        else:
            bb = []

        if self.part_bb_path is not None:
            with h5py.File(self.part_bb_path, "r") as f:
                part_bb = torch.from_numpy(f['aabb'][index]).float()
                if np.abs(part_bb).sum() == 0:
                    index = torch.randint(self.__len__(), (1,)).item()
                    return self.__getitem__(index)
        else:
            part_bb = []

        if self.part_path is not None:
            with h5py.File(self.part_path, "r") as f:
                part = torch.randint(4, (1,)).item()
                with h5py.File(self.part_bb_path, "r") as f_bb:
                    bb_tmp = torch.from_numpy(f_bb['aabb'][index]).float()
                    while bb_tmp[part].sum() == 0:
                        part = torch.randint(4, (1,)).item()  # TODO
                part = torch.from_numpy(f['latent'][index][part])
        else:
            part = []

        with h5py.File(self.latent_path, "r") as f:
            latent = torch.from_numpy(f['latent'][index])

            if self.voxel_path is not None:
                model_id = f['model_id'][index]
                path = self.voxel_path + str(model_id) + '.mat'
                if self.neg_bb:
                    grid = preprocessing.load_hsp(path, 32)[0]
                    neg_bb = util.random_negative_bb(grid, bb, 32)
                else:
                    neg_bb = []
                if self.hull:
                    hull = preprocessing.hull_hsp(path, 32)
                elif self.silhouette:
                    hull = preprocessing.silhouette_hsp(path, 32, torch.randint(3, (1,)).item())
                else:
                    hull = []
            else:
                hull, position, score, neg_bb = [], [], [], []

        return [], [], part, latent, bb, part_bb, neg_bb, hull

    def __len__(self):
        with h5py.File(self.latent_path, "r") as f:
            return f['latent'].shape[0]


class DataGenerationLatent(trainer.DataGeneration):
    def __init__(self, encoder=None, options=None, example=None):
        super().__init__()

        self.options = options
        self.encoder = encoder

        self.fixed_noise = torch.randn((self.options.batch_size, self.options.bottleneck), device=self.options.device)
        self.example = example

    def __call__(self, generator, batch_data, epoch=0):
        instance_noise = max(0.0, (100.0 - float(epoch)) / 100.0) * self.options.instance_noise

        real_latent = batch_data[3].to(self.options.device)

        batch_size = real_latent.shape[0]
        noise = torch.randn((batch_size, self.options.bottleneck), device=self.options.device)
        with torch.no_grad():
            if self.options.mask == "hull" or self.options.mask == "silhouette":
                mask = fn.one_hot(batch_data[7]).permute(0, 4, 1, 2, 3).float().to(self.options.device)
            elif self.options.mask == "bb":
                mask = util.mask_from_bb_batch(batch_data[4], 32).long().to(self.options.device)
                mask = fn.one_hot(mask).permute(0, 4, 1, 2, 3).float()
            elif self.options.mask == "neg_bb":
                mask = util.mask_from_bb_batch(batch_data[4], 32).long().to(self.options.device)
                mask2 = util.mask_from_bb_batch(batch_data[6], 32).long().to(self.options.device)
                mask = torch.max(mask - mask2, torch.zeros_like(mask))
                mask = fn.one_hot(mask).permute(0, 4, 1, 2, 3).float()
            elif self.options.mask == "part_bb":
                mask = util.mask_from_bb_batch_parts(batch_data[5], 32).long().to(self.options.device).float()
            elif self.options.mask == "partial":
                mask = batch_data[2].to(self.options.device)
            else:
                mask = None

        fake_latent = generator(noise, mask)
        real_latent = trainer.augment_data(real_latent, self.options.device, instance_noise)
        fake_latent = trainer.augment_data(fake_latent, self.options.device, instance_noise)

        return real_latent, fake_latent, mask
