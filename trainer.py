import json
import operator
import os
from collections import defaultdict
from itertools import count

import torch
import torch.cuda
import torch.nn.functional as fn
import torch.utils.data
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm


def _sanitize_loss_dict(loss_dict):
    san_dict = {}
    for k in loss_dict:
        val = loss_dict[k]
        if isinstance(val, torch.Tensor) and val.dim() <= 1:
            san_dict[k] = val.item()
        else:
            san_dict[k] = val

    return san_dict


class LossFunction:
    def __init__(self, options=None):
        super().__init__()

        self.options = options

    def __call__(self, model, inputs):
        raise NotImplementedError


class Logger:
    def __init__(self, name, checkpoint_frequency=None, log_tqdm=False,
                 log_tensorboard=False, log_visdom=False, comp_key='total',
                 comp_op=operator.lt, save_models=True, best_train_loss=None,
                 best_val_loss=None, tqdm_precision=3, models_folder='models', logs_folder='logs'):
        self._name = name
        self.checkpoint_frequency = checkpoint_frequency
        self.log_tqdm = log_tqdm
        self.tqdm_precision = tqdm_precision
        self.log_tensorboard = log_tensorboard
        self.log_visdom = log_visdom
        self.save_models = save_models
        self.models_folder = models_folder
        self.logs_folder = logs_folder

        self._train_info = defaultdict(float)
        self._val_info = defaultdict(float)
        self._n_train_batches = 0
        self._n_val_batches = 0

        self._comp_key = comp_key
        self._comp_op = comp_op
        if comp_op(0, 1):
            self._best_train_loss = float("inf")
            self._best_val_loss = float("inf")
        else:
            self._best_train_loss = -float("inf")
            self._best_val_loss = -float("inf")

        if best_train_loss is not None:
            self._best_train_loss = best_train_loss
        if best_val_loss is not None:
            self._best_val_loss = best_val_loss

        if log_tensorboard:
            from torch.utils.tensorboard import SummaryWriter
            logs_path = os.path.join(self.logs_folder, self._name)
            if not os.path.exists(logs_path):
                os.makedirs(logs_path)
            self._writer = SummaryWriter(logs_path)

        models_path = os.path.join(self.models_folder, self._name)
        if not os.path.exists(models_path):
            os.makedirs(models_path)

    def aggregate_info(self, batch_info, train=True, pbar=None):
        if train:
            self._n_train_batches += 1
            for k in batch_info:
                self._train_info[k] += batch_info[k]
        else:
            self._n_val_batches += 1
            for k in batch_info:
                self._val_info[k] += batch_info[k]

        if self.log_tqdm:
            description = "(Training)" if train else "(Validation)"
            if train:
                for k in self._train_info:
                    description += " " + k + ": {:.{p}f}".format(self._train_info[k] / self._n_train_batches,
                                                                 p=self.tqdm_precision)
            else:
                for k in self._val_info:
                    description += " " + k + ": {:.{p}f}".format(self._val_info[k] / self._n_val_batches,
                                                                 p=self.tqdm_precision)

            pbar.set_description(description)

    def log(self, model, optimizer, epoch):
        val_keys = set(self._val_info.keys() if self._val_info is not None else [])
        train_keys = set(self._train_info.keys())

        joined_keys = val_keys & train_keys
        train_keys = train_keys - joined_keys
        val_keys = val_keys - joined_keys

        for key in joined_keys:
            self._train_info[key] /= self._n_train_batches
            self._val_info[key] /= self._n_val_batches
            if self.log_tensorboard:
                self._writer.add_scalars(key, {'train': self._train_info[key],
                                               'validation': self._val_info[key]}, epoch)

        for key in train_keys:
            self._train_info[key] /= self._n_train_batches
            if self.log_tensorboard:
                self._writer.add_scalar(key, self._train_info[key], epoch)

        for key in val_keys:
            self._val_info[key] /= self._n_val_batches
            if self.log_tensorboard:
                self._writer.add_scalar(key, self._val_info[key], epoch)

        if self.save_models:
            assert (self._comp_key in self._train_info)
            if self._comp_op(self._train_info[self._comp_key], self._best_train_loss):
                self._best_train_loss = self._train_info[self._comp_key]
                try:
                    model_path = os.path.join(self.models_folder, self._name, "model_train.state")
                    optim_path = os.path.join(self.models_folder, self._name, "optim_train.state")
                    info_path = os.path.join(self.models_folder, self._name, "info_train.json")
                    torch.save(model.state_dict(), model_path)
                    torch.save(optimizer.state_dict(), optim_path)
                    with open(info_path, 'w') as f:
                        json.dump({'epoch': epoch, 'train': self._train_info, 'val': self._val_info}, f, indent=4)
                except PermissionError:
                    pass

            if len(self._val_info) > 0:
                assert (self._comp_key in self._val_info)
                if self._comp_op(self._val_info[self._comp_key], self._best_val_loss):
                    self._best_val_loss = self._val_info[self._comp_key]
                    try:
                        model_path = os.path.join(self.models_folder, self._name, "model_val.state")
                        optim_path = os.path.join(self.models_folder, self._name, "optim_val.state")
                        info_path = os.path.join(self.models_folder, self._name, "info_val.json")
                        torch.save(model.state_dict(), model_path)
                        torch.save(optimizer.state_dict(), optim_path)
                        with open(info_path, 'w') as f:
                            json.dump({'epoch': epoch, 'train': self._train_info, 'val': self._val_info}, f, indent=4)
                    except PermissionError:
                        pass

            if self.checkpoint_frequency is not None and epoch % self.checkpoint_frequency == 0:
                try:
                    model_path = os.path.join(self.models_folder, self._name, "model_cur.state")
                    optim_path = os.path.join(self.models_folder, self._name, "optim_cur.state")
                    info_path = os.path.join(self.models_folder, self._name, "info_cur.json")
                    torch.save(model.state_dict(), model_path)
                    torch.save(optimizer.state_dict(), optim_path)
                    with open(info_path, 'w') as f:
                        json.dump({'epoch': epoch, 'train': self._train_info, 'val': self._val_info}, f, indent=4)
                except PermissionError:
                    pass

        self._train_info = defaultdict(float)
        self._val_info = defaultdict(float)
        self._n_train_batches = 0
        self._n_val_batches = 0


class Trainer:
    def __init__(self, model, loss_fn, optimizer, train_data_loader,
                 val_data_loader=None, test_data_loader=None, eval_fn=None,
                 logger=None, start_epoch=0):
        self.model = model
        self.loss_fn = loss_fn
        self.optimizer = optimizer
        self.train_data_loader = train_data_loader
        self.val_data_loader = val_data_loader
        self.test_data_loader = test_data_loader
        self.start_epoch = start_epoch
        self.logger = logger
        self.eval_fn = loss_fn if eval_fn is None else eval_fn

    def train(self, n_epochs=1):
        use_tqdm = self.logger is not None and self.logger.log_tqdm
        epoch_range = range(self.start_epoch, self.start_epoch + n_epochs)
        for e in epoch_range:
            self.model.train()
            data_loader = self.train_data_loader if not use_tqdm else tqdm(self.train_data_loader, dynamic_ncols=True)
            for batch_data in data_loader:
                self.optimizer.zero_grad()
                loss_dict = self.loss_fn(self.model, batch_data)
                if 'total' in loss_dict:
                    loss_dict['total'].backward()
                self.optimizer.step()

                self.logger.aggregate_info(_sanitize_loss_dict(loss_dict),
                                           train=True, pbar=data_loader if use_tqdm else data_loader)

            if self.val_data_loader is not None:
                self.model.eval()
                with torch.no_grad():
                    data_loader = self.val_data_loader if not use_tqdm else tqdm(self.val_data_loader,
                                                                                 dynamic_ncols=True)
                    for batch_data in data_loader:
                        val_dict = self.eval_fn(self.model, batch_data)
                        self.logger.aggregate_info(_sanitize_loss_dict(val_dict), train=False,
                                                   pbar=data_loader if use_tqdm else data_loader)

            if self.logger is not None:
                self.logger.log(self.model, self.optimizer, e)

    def restore(self, path, checkpoint, restore_mode="complete"):
        assert (restore_mode in ["complete", "model_only"])
        assert (checkpoint in ["train", "val", "cur"])

        if checkpoint == "train":
            model_path = os.path.join(path, "model_train.state")
            optim_path = os.path.join(path, "optim_train.state")
            info_path = os.path.join(path, "info_train.json")
        elif checkpoint == "val":
            model_path = os.path.join(path, "model_val.state")
            optim_path = os.path.join(path, "optim_val.state")
            info_path = os.path.join(path, "info_val.json")
        else:
            model_path = os.path.join(path, "model_cur.state")
            optim_path = os.path.join(path, "optim_cur.state")
            info_path = os.path.join(path, "info_cur.json")

        self.model.load_state_dict(torch.load(model_path))
        if restore_mode == "complete":
            self.optimizer.load_state_dict(torch.load(optim_path))
            with open(info_path) as json_file:
                self.start_epoch = json.load(json_file)['epoch']


def get_label(shape, device, soft, noisy):
    real_label = torch.ones(shape, device=device)
    fake_label = torch.zeros(shape, device=device)

    if soft:
        real_label -= (torch.rand(shape, device=device) * 0.2)
        # unclear which version is better
        # real_label += (torch.rand(shape, device=device) * 0.5) - 0.3
        # fake_label += (torch.rand(shape, device=device) * 0.3)

    if noisy:
        mask1 = torch.rand(shape, device=device) > torch.tensor([0.95], device=device)
        mask2 = torch.rand(shape, device=device) > torch.tensor([0.95], device=device)
        tmp = fake_label[mask1]
        fake_label[mask2] = real_label[mask2]
        real_label[mask1] = tmp

    return real_label, fake_label


def augment_data(batch_data, device, instance_noise=0, random_points=0):
    if instance_noise > 0:
        batch_data = batch_data + torch.randn_like(batch_data) * instance_noise

    if random_points > 0:
        b = batch_data.shape[0]
        batch_data = torch.cat([batch_data, torch.rand(b, 3, random_points, device=device) - 0.5], dim=2)

    return batch_data


def feature_matching(real_feature, fake_feature):
    real_feature = real_feature.mean(dim=0)
    fake_feature = fake_feature.mean(dim=0)
    loss = fn.mse_loss(real_feature, fake_feature)

    return loss


def gradient_regularization(batch_data, output, center, reduction='mean'):
    batch_size = batch_data.shape[0]
    grad = torch.autograd.grad(
        # outputs=output.view(batch_size, -1).mean(-1).sum(),
        outputs=output.sum(),
        inputs=batch_data,
        create_graph=True,
        only_inputs=True
    )[0]

    grad_norm = ((grad.view(batch_size, -1).norm(2, dim=1) - center) ** 2)

    if reduction is 'max':
        grad_norm = grad_norm.max()
    elif reduction is 'sum':
        grad_norm = grad_norm.sum()
    else:
        grad_norm = grad_norm.mean()

    return grad_norm


def wgan_loss_critic(output_real, output_fake):
    return (output_fake - output_real).mean()


def hinge_loss_critic(output_real, output_fake):
    loss_real = -torch.min(torch.zeros_like(output_real), -torch.ones_like(output_real) + output_real).mean()
    loss_fake = -torch.min(torch.zeros_like(output_fake), -torch.ones_like(output_fake) - output_fake).mean()
    return loss_real + loss_fake


def ns_loss_critic(output_real, output_fake, soft_labels=False, noisy_labels=False, reduce=None):
    output_real, output_fake = torch.sigmoid(output_real), torch.sigmoid(output_fake)
    if reduce == 'mean':
        b = output_real.shape[0]
        output_real, output_fake = output_real.view(b, -1).mean(-1), output_fake.view(b, -1).mean(-1)
    real_label, fake_label = get_label(output_fake.shape, output_fake.device, soft_labels, noisy_labels)
    loss_d_real = fn.binary_cross_entropy(output_real, real_label)
    loss_d_fake = fn.binary_cross_entropy(output_fake, fake_label)
    loss_d = loss_d_fake + loss_d_real
    return loss_d


def gan_loss_critic(output_real, output_fake, soft_labels=False, noisy_labels=False, reduce=None):
    output_real, output_fake = torch.sigmoid(output_real), torch.sigmoid(torch.ones_like(output_fake) - output_fake)
    if reduce == 'mean':
        b = output_real.shape[0]
        output_real, output_fake = output_real.view(b, -1).mean(-1), output_fake.view(b, -1).mean(-1)
    real_label, fake_label = get_label(output_fake.shape, output_fake.device, soft_labels, noisy_labels)
    loss_d_real = fn.binary_cross_entropy(output_real, real_label)
    loss_d_fake = fn.binary_cross_entropy(output_fake, real_label)
    loss_d = loss_d_fake + loss_d_real
    return loss_d


def wgan_loss_generator(output_fake):
    loss = -output_fake.mean()
    return loss


def hinge_loss_generator(output_fake):
    loss = -output_fake.mean()
    return loss


def ns_loss_generator(output_fake, reduce=None):
    output_fake = torch.sigmoid(output_fake)
    if reduce == 'mean':
        b = output_fake.shape[0]
        output_fake = output_fake.view(b, -1).mean(-1)
    loss = fn.binary_cross_entropy(output_fake, torch.ones_like(output_fake))
    return loss


def feature_loss_generator(feature_real, feature_fake):
    feature_real = torch.mean(feature_real, 0)
    feature_fake = torch.mean(feature_fake, 0)
    loss = fn.mse_loss(feature_fake, feature_real.detach())
    return loss


def gan_loss_generator(output_fake, reduce=None):
    output_fake = torch.sigmoid(output_fake)
    if reduce == 'mean':
        b = output_fake.shape[0]
        output_fake = output_fake.view(b, -1).mean(-1)
    output_fake = torch.sigmoid(output_fake)
    loss = - fn.binary_cross_entropy((torch.ones_like(output_fake) - output_fake), torch.ones_like(output_fake))
    return loss


class DataGeneration:
    def __init__(self, encoder=None, options=None):
        self.options = options
        self.encoder = encoder

    def __call__(self, generator, batch_data, epoch=0):
        raise NotImplementedError
    #
    # def create_example(self, generator):
    #     raise NotImplementedError


class LossFunctionCritic:
    _id = count(0)

    def __init__(self, options=None):
        self.id = next(self._id)
        self.options = options

    def __call__(self, critic, real_data, fake_data, add_data):
        raise NotImplementedError


class LossFunctionGenerator:
    def __init__(self, options=None):
        self.options = options

    def __call__(self, critic, real_data, fake_data, add_data):
        raise NotImplementedError


class GANLogger:
    def __init__(self, name, checkpoint_frequency=None, log_tqdm=False, log_tensorboard=False, tqdm_precision=3):
        self._name = name
        self.checkpoint_frequency = checkpoint_frequency
        self.log_tqdm = log_tqdm
        self.log_tensorboard = log_tensorboard
        self.tqdm_precision = tqdm_precision

        self._info = defaultdict(float)
        self._n_batches = 0

        models_path = os.path.join("models", self._name)
        if not os.path.exists(models_path):
            os.makedirs(models_path)

        if log_tensorboard:
            logs_path = os.path.join("logs_GAN", self._name)
            if not os.path.exists(logs_path):
                os.makedirs(logs_path)
            self._writer = SummaryWriter(logs_path)

    def aggregate_info(self, epoch, batch_info, pbar=None):
        self._n_batches += 1
        for k in batch_info:
            self._info[k] += batch_info[k]

        if self.log_tqdm:
            loss_d = self._info["loss_d"] / self._n_batches
            loss_g = self._info["loss_g"] / self._n_batches
            out_fake = self._info["output_fake"] / self._n_batches
            out_fake2 = self._info["output_fake2"] / self._n_batches
            out_real = self._info["output_real"] / self._n_batches
            grad_norm = self._info["grad_norm"] / self._n_batches

            loss_description = '[{}] Loss_D: {:.{p}f} Loss_G: {:.{p}} Reg_D: {:.{p}f}'.format(
                epoch, loss_d, loss_g, grad_norm, p=self.tqdm_precision)
            info_description = ' (D(x): {:.4f} D(G(z)): {:.4f} D+1(G(z)): {:.4f})'.format(
                out_real, out_fake, out_fake2, p=self.tqdm_precision)
            description = loss_description + info_description

            pbar.set_description(description)

    def log(self, generator, critic, optimizer_gen, optimizer_critic, epoch):
        if self.log_tensorboard:
            for key in set(self._info.keys()):
                self._info[key] /= self._n_batches
                self._writer.add_scalar(key, self._info[key], epoch)

        if self.checkpoint_frequency is not None and (epoch + 1) % self.checkpoint_frequency == 0:
            try:
                models_path = os.path.join("models", self._name, "epoch_" + str(epoch + 1))
                if not os.path.exists(models_path):
                    os.makedirs(models_path)
                generator_path = os.path.join(models_path, "generator.state")
                optim_gen_path = os.path.join(models_path, "optim_generator.state")
                critic_path = os.path.join(models_path, "critic.state")
                optim_critic_path = os.path.join(models_path, "optim_critic.state")
                info_path = os.path.join(models_path, "info.json")
                torch.save(generator.state_dict(), generator_path)
                torch.save(optimizer_gen.state_dict(), optim_gen_path)
                torch.save(critic.state_dict(), critic_path)
                torch.save(optimizer_critic.state_dict(), optim_critic_path)

                with open(info_path, 'w') as f:
                    json.dump({'epoch': epoch, 'train': self._info}, f, indent=4)
            except PermissionError:
                pass

        self._info = defaultdict(float)
        self._n_batches = 0


class GANTrainer:
    def __init__(self, generator, critic, data_generation, loss_generator, loss_critic,
                 optimizer_generator, optimizer_critic,
                 train_data_loader, val_data_loader=None, logger=None):
        self.generator = generator
        self.critic = critic
        self.data_generation = data_generation
        self.loss_generator = loss_generator
        self.loss_critic = loss_critic
        self.optimizer_generator = optimizer_generator
        self.optimizer_critic = optimizer_critic
        self.train_data_loader = train_data_loader
        self.val_data_loader = val_data_loader
        self.logger = logger
        self.start_epoch = 0

    def train(self, n_epochs=1):
        use_tqdm = self.logger is not None and self.logger.log_tqdm
        epoch_range = range(self.start_epoch, n_epochs)
        for e in epoch_range:
            self.generator.train()
            self.critic.train()
            data_loader = self.train_data_loader if not use_tqdm else tqdm(self.train_data_loader, dynamic_ncols=True)
            for batch_data in data_loader:
                real_data, fake_data, add_data = self.data_generation(self.generator, batch_data, e)

                self.critic.zero_grad()
                critic_loss_dict, loss = self.loss_critic(self.critic, real_data, fake_data.detach(), add_data)
                loss.backward()
                self.optimizer_critic.step()

                self.generator.zero_grad()
                generator_loss_dict, loss = self.loss_generator(self.critic, real_data, fake_data, add_data)
                loss.backward()
                self.optimizer_generator.step()

                loss_dict = {**generator_loss_dict, **critic_loss_dict}
                self.logger.aggregate_info(e, _sanitize_loss_dict(loss_dict), pbar=data_loader)

            if self.logger is not None:
                self.logger.log(self.generator, self.critic, self.optimizer_generator, self.optimizer_critic, e)

    def restore(self, path, restore_mode="complete"):
        assert (restore_mode in ["complete", "model_only"])

        model_generator_path = os.path.join(path, "generator.state")
        model_critic_path = os.path.join(path, "critic.state")
        optim_generator_path = os.path.join(path, "optim_generator.state")
        optim_critic_path = os.path.join(path, "optim_critic.state")
        info_path = os.path.join(path, "info.json")

        self.generator.load_state_dict(torch.load(model_generator_path))
        self.critic.load_state_dict(torch.load(model_critic_path))
        if restore_mode == "complete":
            self.optimizer_generator.load_state_dict(torch.load(optim_generator_path))
            self.optimizer_critic.load_state_dict(torch.load(optim_critic_path))
            with open(info_path) as json_file:
                self.start_epoch = json.load(json_file)['epoch']
