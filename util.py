import os
import pickle
import random

import numpy as np
import torch


def geometry_in_bb(grid, bb, resolution):
    offset = 1 / float(resolution) / 2.0
    samples = grid.nonzero(as_tuple=False) / float(resolution) - 0.5
    inside = inside_aabb(samples.transpose(1, 0), bb, offset)
    return inside.shape[0] > 0


def random_negative_bb(grid, bb, res):
    mean = (bb[:, 1] + bb[:, 0]) / 2.0
    scale = bb[:, 1] - bb[:, 0]
    empty_bb = torch.zeros((3, 2))
    for i in range(10):
        for j in range(10):
            sample = ((((torch.rand(3, ) - 0.5) * scale - mean) + 0.5) * res).long()
            score = grid[sample[0], sample[1], sample[2]]
            if score < 0.5:
                break
        sample = sample / float(res) - 0.5
        scale = torch.rand(3, ) * 0.1 + 1.0 / float(res)
        candidate = torch.stack([sample - scale, sample + scale], dim=1)
        if not geometry_in_bb(grid, candidate, res):
            empty_bb = candidate
            break
    return empty_bb


def split_grid(grid, size, padding):
    grid_size = grid.shape[-1]
    batch_size = grid.shape[0]
    inner = size - padding
    filled = size + 2 * padding
    n_chunks = grid_size // size
    n_inner = n_chunks - 1

    chunks = torch.stack(torch.split(grid, size, dim=1), dim=1)
    zero_padding = torch.zeros(batch_size, 1, padding, grid_size, grid_size).to(chunks)
    boundary_right = torch.cat([chunks[:, 1:, :padding, :, :], zero_padding], dim=1)
    boundary_left = torch.cat([zero_padding, chunks[:, :n_inner, inner:, :, :]], dim=1)
    chunks = torch.cat([boundary_left, chunks, boundary_right], dim=2)

    chunks = torch.stack(torch.split(chunks, size, dim=3), dim=2)
    zero_padding = torch.zeros(batch_size, n_chunks, 1, filled, padding, grid_size).to(chunks)
    boundary_right = torch.cat([chunks[:, :, 1:, :, :padding, :], zero_padding], dim=2)
    boundary_left = torch.cat([zero_padding, chunks[:, :, :n_inner, :, inner:, :]], dim=2)
    chunks = torch.cat([boundary_left, chunks, boundary_right], dim=4)

    chunks = torch.stack(torch.split(chunks, size, dim=5), dim=3)
    zero_padding = torch.zeros(batch_size, n_chunks, n_chunks, 1, filled, filled, padding).to(chunks)
    boundary_right = torch.cat([chunks[:, :, :, 1:, :, :, :padding], zero_padding], dim=3)
    boundary_left = torch.cat([zero_padding, chunks[:, :, :, :n_inner, :, :, inner:]], dim=3)
    chunks = torch.cat([boundary_left, chunks, boundary_right], dim=6)

    chunks = chunks.view(-1, filled, filled, filled)
    return chunks


def meshgrid(s, device=torch.device('cpu')):
    r = torch.arange(s, device=device, dtype=torch.float)
    x = r[:, None, None].expand(s, s, s)
    y = r[None, :, None].expand(s, s, s)
    z = r[None, None, :].expand(s, s, s)
    return torch.stack([x, y, z], 0) / (s - 1) - 0.5


def normalize_unit_cube(points):
    bb_max = points.max(-1)[0]
    bb_min = points.min(-1)[0]
    length = (bb_max - bb_min).max()
    mean = (bb_max + bb_min) / 2.0
    scale = 1.0 / length
    res = (points - mean.unsqueeze(1)) * scale
    return res.clamp(-0.5, 0.5)


def normalize_unit_sphere(points):
    mean = torch.mean(points, dim=-1)
    out = points - mean.unsqueeze(-1)
    radius = torch.norm(out, dim=-2).max(dim=-1)[0]
    out = out / (radius * 2)
    return out


def transform_to_bb(points, aabb):
    tmp = points.copy()
    points[:, 0] = tmp[:, 2]
    points[:, 2] = tmp[:, 0]
    min_val = points.min(axis=0)
    max_val = points.max(axis=0)
    scale = max_val - min_val
    center = (min_val + max_val) / 2.0
    pres_scale = np.array([aabb[0, 1] - aabb[0, 0], aabb[1, 1] - aabb[1, 0], aabb[2, 1] - aabb[2, 0]])
    pres_center = np.array([(aabb[0, 1] + aabb[0, 0]) / 2.0,
                            (aabb[1, 1] + aabb[1, 0]) / 2.0,
                            (aabb[2, 1] + aabb[2, 0]) / 2.0])

    translation = pres_center - center
    scaling = pres_scale / scale

    points += translation
    points *= scaling


def jitter(points, resolution):
    out = points + (torch.rand(points.shape) - 0.5) / resolution
    return out


def inside_aabb(samples, bb, offset=0.0):
    inside_x1 = samples[0] > bb[0, 0] - offset
    inside_x2 = samples[0] < bb[0, 1] + offset
    inside_y1 = samples[1] > bb[1, 0] - offset
    inside_y2 = samples[1] < bb[1, 1] + offset
    inside_z1 = samples[2] > bb[2, 0] - offset
    inside_z2 = samples[2] < bb[2, 1] + offset

    inside = torch.min(inside_x1, inside_x2)
    inside = torch.min(inside, inside_y1)
    inside = torch.min(inside, inside_y2)
    inside = torch.min(inside, inside_z1)
    inside = torch.min(inside, inside_z2)

    return inside.squeeze().nonzero(as_tuple=False)


def bb_from_voxel(voxel):
    g = voxel.shape[-1]
    voxel = voxel.squeeze()
    nonzero = voxel.nonzero(as_tuple=False) / float(g) - 0.5
    bb_max = nonzero.max(0)[0] + (1 / float(g))
    bb_min = nonzero.min(0)[0]

    bb = torch.empty([3, 2])
    bb[0, 0] = bb_min[0]
    bb[0, 1] = bb_max[0]
    bb[1, 0] = bb_min[1]
    bb[1, 1] = bb_max[1]
    bb[2, 0] = bb_min[2]
    bb[2, 1] = bb_max[2]
    return bb


def mask_from_bb_batch(bb, resolution, offset_scale="half"):
    masks = []
    for i in range(bb.shape[0]):
        mask = mask_from_bb(bb[i], resolution, offset_scale)
        masks.append(mask)
    return torch.stack(masks)


def mask_from_bb_batch_parts(bb, resolution, offset_scale="half"):
    masks = []
    for i in range(bb.shape[0]):
        mask = mask_from_bb_parts(bb[i], resolution, offset_scale)
        masks.append(mask)
    return torch.stack(masks)


def mask_from_bb(bb, resolution, offset_scale="half"):
    device = bb.device
    samples = meshgrid(resolution, device=device) * (resolution - 1) / resolution
    mask = torch.zeros(resolution, resolution, resolution, dtype=torch.bool, device=device)

    if bb.abs().sum() == 0:
        return mask.bool()

    offset = 1 / float(resolution)
    if offset_scale == "half":
        offset /= 2.0

    inside = inside_aabb(samples, bb, offset)
    mask[inside[:, 0], inside[:, 1], inside[:, 2]] = 1

    return mask.bool()


def mask_from_bb_parts(bb, resolution, offset_scale="half"):
    device = bb.device
    samples = meshgrid(resolution, device=device) * (resolution - 1) / resolution
    mask = torch.zeros((bb.shape[0], resolution, resolution, resolution), dtype=torch.bool, device=device)

    offset = 1 / float(resolution)
    if offset_scale == "half":
        offset /= 2.0

    for i in range(bb.shape[0]):
        if bb[i].abs().sum() == 0:
            continue
        inside = inside_aabb(samples, bb[i], offset)
        mask[i, inside[:, 0], inside[:, 1], inside[:, 2]] = 1

    return mask.bool()


# input is expected in form (b x) c x n
def dist_mat_squared(x, y):
    assert x.dim() == 3 or x.dim() == 2

    xx = torch.sum(x ** 2, dim=-2).unsqueeze(-1)
    yy = torch.sum(y ** 2, dim=-2)
    if x.dim() == 3:
        yy = yy.unsqueeze(-2)
        dists = torch.bmm(x.transpose(2, 1), y)
    else:
        dists = torch.matmul(x.t(), y)
    dists *= -2
    dists += yy
    dists += xx

    return dists


def dist_norm_p(x, y, p=2):
    d = dist_mat_squared(x, y)
    if x.dim() == 2:
        dists_1 = (x - y[:, d.min(-1)[1]]).norm(dim=0, p=p)
        dists_2 = (x[:, d.min(-2)[1]] - y).norm(dim=0, p=p)
    else:  # dim is 3
        b_d_ind_1 = d.min(-1)[1]
        b_d_ind_2 = d.min(-2)[1]
        b_dists_1 = []
        b_dists_2 = []
        for i in range(b_d_ind_1.shape[0]):
            b_dists_1.append((x[i] - y[i, :, b_d_ind_1[i]]).norm(dim=0, p=p))
            b_dists_2.append((x[i, :, b_d_ind_2[i]] - y[i]).norm(dim=0, p=p))
        dists_1 = torch.stack(b_dists_1)
        dists_2 = torch.stack(b_dists_2)
    return dists_1, dists_2


def chamfer(x, y):
    dists_1, dists_2 = dist_norm_p(x, y, 2)
    return dists_1.mean(dim=-1) + dists_2.mean(dim=-1)


def save_params(opt):
    if not os.path.exists(os.path.join("models", opt.run_name)):
        os.makedirs(os.path.join("models", opt.run_name))
    # if not os.path.exists(os.path.join("examples", opt.run_name)):
    #     os.makedirs(os.path.join("examples", opt.run_name))
    with open(os.path.join("models", opt.run_name, 'opt.pkl'), 'wb') as f:
        pickle.dump(opt, f)


def set_random_seed(seed, deterministic=False):
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    if deterministic:
        torch.set_deterministic(True)
