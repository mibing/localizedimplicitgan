requirements:
pytorch
numpy
scipy
h5py
trimesh
skimage
tqdm
tensorboard

download voxelized models from https://github.com/chaene/hsp

VOXEL_PATH = path to downloaded voxelized models
DATA_PATH = folder where all intermediate results are stored
MODEL_PATH = path to models folder
LATENT_PATH = path to precomputed latent vectors
BB_PATH = path to precomputed bounding boxes


prepare bounding boxes for conditional generation:
python  preparation.py --bounding-box --class-label '03001627' --voxel-path {VOXEL_PATH} --dataset-path {DATA_PATH}

train autoencoder (to train on complete dataset choose LABEL=complete:
python train_autoencoder.py --data-path {VOXEL_PATH}--run-name {AE_NAME} --n-epochs 200 --batch-size 16 --bottleneck 8 --arch 'high-res' --interpolate --class-label {LABEL}

prepare dataset in latent representation (this creates some temporary files, that need to be removed manually)
python preparation.py --truth-latent --mode 'train' --voxel-path {VOXEL_PATH} --model-path {MODEL_PATH} --dataset-path {DATA_PATH} --ae-name {AE_NAME} --latent-dim 8 --batch-size 4 --interpolate --class-label {LABEL}

train GAN:
unconditional
python train_gan.py --run-name {GAN_NAME} --data-path {LATENT_PATH} --latent-dim 8  --batch-size 48 --gradient-penalty 1.0 --instance-noise 0.0 --penalty-center 0.0 --penalty-reduction 'max' --loss 'NS' --generator-arch 'convolutional'

conditional
python train_gan.py --run-name {GAN_NAME} --data-path {LATENT_PATH} --bb-path {BB_PATH} --latent-dim 8 --batch-size 20 --gradient-penalty 1.0 --instance-noise 0.0 --penalty-center 0.0 --penalty-reduction 'max' --loss 'NS' --mask 'bb' --generator-arch 'conditional'

create objects from trained GAN
python preparation.py --gan-mesh --resolution 256 --latent-dim 8 --dataset-path {DATA_PATH} --model-path {MODEL_PATH} --ae-name {AE_NAME} --gan-name {GAN_NAME} --epoch 500 --n-objects 6780 --interpolate --gan-arch 'convolutional'


to compute the Light Field Descriptor we use the code from https://github.com/Sunwinds/ShapeDescriptor/tree/master/LightField/3DRetrieval_v1.8/3DRetrieval_v1.8

The executable only does shape retrieval based on the LFD. To get the distances the C code needs to be rewritten.