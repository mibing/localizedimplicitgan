import torch
import torch.nn as nn
import torch.nn.functional as fn

import util


class ImplicitCheck(nn.Module):
    def __init__(self, classifier):
        super(self.__class__, self).__init__()

        self.classifier = classifier

    def forward(self, code, points):
        b, c, g, _, _ = code.shape

        # points are expected to be in range -0.5 - 0.5
        assert (points.min() >= -0.5)
        assert (points.max() <= 0.5)

        points = (points + 0.5) * g
        indices = points.floor().clamp_(0, g - 1)
        points = points - indices - 0.5  # offset vectors in range [-0.5,0.5)
        indices = (indices[:, 0, :] * (g ** 2) + indices[:, 1, :] * g + indices[:, 2, :]).long()

        # gather code corresponding to each point
        code = code.view(b, c, -1)
        code = torch.gather(code, 2, indices.unsqueeze(1).repeat([1, c, 1]))

        inp = torch.cat([code, points], dim=1)
        out = self.classifier(inp)

        return out


class ImplicitCheckInterpolate(nn.Module):
    def __init__(self, classifier=None, interpolation_range=1.0):
        super(self.__class__, self).__init__()

        self.classifier = classifier
        self.interp_range = interpolation_range

    def forward(self, code, points):
        b, c, g, _, _ = code.shape
        code = fn.pad(code, [1, 1, 1, 1, 1, 1], "replicate")

        # points are expected to be in range -0.5 - 0.5
        assert (points.min() >= -0.5)
        assert (points.max() <= 0.5)

        points = (points + 0.5) * g
        indices = points.floor().clamp_(0, g - 1)

        # expand to neighbourhood
        indices = indices[:, :, None, None, None, :].repeat([1, 1, 3, 3, 3, 1]) \
                  + (util.meshgrid(3) * 2)[None, :, :, :, :, None].to(indices.device)

        indices = indices.view(b, 3, -1) + 1  # indices in range [0,g+1]
        points = points[:, :, None, None, None, :].repeat(1, 1, 3, 3, 3, 1).view(b, 3, -1)
        points = points - indices + 0.5  # offset vectors in range [-1.5,1.5)
        indices = (indices[:, 0, :] * ((g + 2) ** 2) + indices[:, 1, :] * (g + 2) + indices[:, 2, :]).long()

        # gather code corresponding to each point
        code = code.view(b, c, -1)
        code = torch.gather(code, 2, indices.unsqueeze(1).repeat([1, c, 1]))

        inp = torch.cat([code, points], dim=1)
        out = self.classifier(inp)

        out = out.view(b, 3, 3, 3, -1)

        if self.training:  # during training we do not want to blend, but train each value within range
            weight = points.abs() < self.interp_range
            weight = weight[:, 0, :] * weight[:, 1, :] * weight[:, 2, :]
            weight = weight.view(b, 3, 3, 3, -1).detach()
            out = out * weight
        else:
            weight = (self.interp_range - points.clamp(-self.interp_range, self.interp_range).abs())
            weight = weight[:, 0, :] * weight[:, 1, :] * weight[:, 2, :]
            weight = weight.view(b, 3, 3, 3, -1).detach()
            out = (out * weight).sum(1).sum(1).sum(1) / weight.sum(1).sum(1).sum(1)

        # check = weight.sum(1).sum(1).sum(1)
        # assert(check.min() == 1.0 and check.max() == 1.0)

        return out


class ResidualBlock(nn.Module):
    def __init__(self, in_channel, out_channel, kernel_size=3, stride=1, normalization='spectral'):
        super(self.__class__, self).__init__()

        self.num_features = in_channel
        self.res = False
        if stride is not 1:
            self.downsample = True
        else:
            self.downsample = False

        if normalization == 'spectral':
            self.conv = nn.Sequential(
                torch.nn.utils.spectral_norm(nn.Conv3d(in_channel, out_channel, kernel_size, stride, 1),
                                             n_power_iterations=5),
                nn.LeakyReLU(),
                torch.nn.utils.spectral_norm(nn.Conv3d(out_channel, out_channel, 3, 1, 1),
                                             n_power_iterations=5))
            if in_channel is not out_channel:
                self.res = True
                self.residual = torch.nn.utils.spectral_norm(nn.Conv3d(in_channel, out_channel, 1, 1, 0),
                                                             n_power_iterations=5)
        elif normalization == 'batch':
            self.conv = nn.Sequential(
                nn.Conv3d(in_channel, out_channel, kernel_size, stride, 1, bias=False),
                nn.BatchNorm3d(out_channel),
                nn.LeakyReLU(),
                nn.Conv3d(out_channel, out_channel, 3, 1, 1, bias=False),
                nn.BatchNorm3d(out_channel))
            if in_channel is not out_channel:
                self.res = True
                self.residual = nn.Sequential(
                    nn.Conv3d(in_channel, out_channel, 1, 1, 0, bias=False),
                    nn.BatchNorm3d(out_channel))
        else:
            self.conv = nn.Sequential(
                nn.Conv3d(in_channel, out_channel, kernel_size, stride, 1),
                nn.LeakyReLU(),
                nn.Conv3d(out_channel, out_channel, 3, 1, 1))
            if in_channel is not out_channel:
                self.res = True
                self.residual = nn.Conv3d(in_channel, out_channel, 1, 1, 0)

    def forward(self, x):
        out = self.conv(x)

        if self.downsample:
            res = fn.max_pool3d(x, kernel_size=2)
            if self.res:
                res = self.residual(res)
        else:
            if self.res:
                res = self.residual(x)
            else:
                res = x
        out = out + res

        return fn.leaky_relu(out)


class SPADELayer(nn.Module):
    def __init__(self, channel, mask_channel, max_pool=False):
        super(self.__class__, self).__init__()
        self.max_pool = max_pool
        self.num_features = channel
        self.normalization = nn.BatchNorm3d(channel)
        self.preprocessing = nn.Sequential(
            nn.Conv3d(mask_channel, 128, 3, padding=1),
            nn.ReLU()
        )
        self.estimate_scale = nn.Sequential(
            nn.ReplicationPad3d(1),
            nn.Conv3d(128, channel, 3, padding=0)
        )
        self.estimate_bias = nn.Sequential(
            nn.ReplicationPad3d(1),
            nn.Conv3d(128, channel, 3, padding=0)
        )

    def forward(self, x, mask):
        if self.max_pool:
            mask = fn.adaptive_max_pool3d(mask.float(), x.shape[-1])
        else:
            mask = fn.interpolate(mask.float(), size=x.shape[-1], mode='trilinear', align_corners=False)
        mask = self.preprocessing(mask)
        scale = self.estimate_scale(mask)
        bias = self.estimate_bias(mask)

        x = self.normalization(x)
        x = x * scale + bias

        return x


class SPADEResidualBlock(nn.Module):
    def __init__(self, in_channel, out_channel, mask_channel, max_pool=False):
        super(self.__class__, self).__init__()

        self.num_features = in_channel
        self.spade1 = SPADELayer(in_channel, mask_channel, max_pool)
        self.conv1 = nn.Conv3d(in_channel, out_channel, 3, padding=1)
        self.spade2 = SPADELayer(out_channel, mask_channel, max_pool)
        self.conv2 = nn.Conv3d(out_channel, out_channel, 3, padding=1, bias=False)

        if in_channel is not out_channel:
            self.res = True
            self.residual = nn.Sequential(
                SPADELayer(in_channel, mask_channel, max_pool),
                nn.ReLU(),
                nn.Conv3d(in_channel, out_channel, 3, padding=1, bias=False)
            )
        else:
            self.res = False

    def forward(self, x, mask):
        out = self.conv1(fn.relu(self.spade1(x, mask)))
        out = self.conv2(fn.relu(self.spade2(out, mask)))

        if self.res:
            res = self.residual[2](self.residual[1](self.residual[0](x, mask)))
        else:
            res = x

        out = out + res

        return out


class ConditionalDecoder(nn.Module):
    def __init__(self, decoder, noise=False, skip=False, out_shape=None):
        super(self.__class__, self).__init__()
        assert (isinstance(decoder, nn.ModuleList))

        self.decoder = decoder

        self.norm_indices = []
        self.noise = noise
        self.skip = skip
        self.out_shape = out_shape

        if noise:
            self.noise_layer = nn.ModuleList([])
        if skip:
            self.skip_layer = nn.ModuleList([])

        for i, l in enumerate(self.decoder):
            if isinstance(l, SPADELayer) or isinstance(l, SPADEResidualBlock):
                if self.noise:
                    self.noise_layer.append(nn.Conv3d(1, l.num_features, 1, bias=True))
                if self.skip:
                    self.skip_layer.append(nn.Conv3d(l.conv2.out_channels, out_shape[0], 1, bias=True))
                self.norm_indices.append(i)

    def forward(self, x, mask):
        j = 0
        b = x.shape[0]

        if self.skip:
            out = torch.zeros(self.out_shape, device=x.device)

        for i, l in enumerate(self.decoder):
            if j < len(self.norm_indices) and i == self.norm_indices[j]:
                if self.noise:
                    grid_size = x.shape[-1]
                    noise_cube = torch.randn(b, 1, grid_size, grid_size, grid_size).to(x)
                    noise_cube = self.noise_layer[j](noise_cube)
                    x = x + noise_cube

                x = l(x, mask)

                if self.skip:
                    out = out + fn.interpolate(self.skip_layer[j](x), size=self.out_shape[-1], mode='trilinear',
                                               align_corners=False)

                j += 1
            else:
                x = l(x)

        if self.skip:
            return out

        return x
