import argparse
import os
import pickle

import torch
import torch.cuda
import torch.utils.data

import datasets
import losses
import models
import trainer
import util


def train(opt):
    util.save_params(opt)

    if opt.mask == 'bb':
        mask_channel = 2
        train_data = datasets.ShapeNetPrecomputed(opt.data_path,
                                                  bb_path=opt.bb_path,
                                                  neg_bb=False)
    elif opt.mask == 'neg_bb':
        mask_channel = 2
        train_data = datasets.ShapeNetPrecomputed(opt.data_path,
                                                  bb_path=opt.bb_path,
                                                  voxel_path=opt.voxel_path,
                                                  neg_bb=True)
    elif opt.mask == 'part_bb':
        mask_channel = 6
        train_data = datasets.ShapeNetPrecomputed(opt.data_path,
                                                  part_bb_path=opt.bb_path)
    elif opt.mask == 'hull':
        mask_channel = 2
        train_data = datasets.ShapeNetPrecomputed(opt.data_path,
                                                  voxel_path=opt.voxel_path, hull=True)
    elif opt.mask == 'silhouette':
        mask_channel = 2
        train_data = datasets.ShapeNetPrecomputed(opt.data_path,
                                                  voxel_path=opt.voxel_path, silhouette=True)
    elif opt.mask == 'partial':
        mask_channel = opt.latent_dim
        train_data = datasets.ShapeNetPrecomputed(opt.data_path,
                                                  part_path=opt.part_path,
                                                  part_bb_path=opt.bb_path)
    else:
        mask_channel = 0
        train_data = datasets.ShapeNetPrecomputed(opt.data_path)

    train_dataloader = torch.utils.data.DataLoader(train_data,
                                                   batch_size=opt.batch_size,
                                                   shuffle=True,
                                                   drop_last=False)

    if opt.mask == 'partial':
        ae = models.load_auto_encoder(opt.ae_arch, opt.latent_dim,
                                      path=opt.ae_path,
                                      interpolate=True, grid=True).eval().to(opt.device)
        data_generation = datasets.DataGenerationLatent(encoder=ae, options=opt)
    else:
        data_generation = datasets.DataGenerationLatent(encoder=None, options=opt)

    generator = models.load_generator(opt.generator_arch, opt.latent_dim, opt.bottleneck,
                                      mask_channel, None, opt.parallel).to(opt.device)

    critic = models.DiscriminatorStandard(opt.latent_dim, mask_channel).to(opt.device)

    optimizer_g = torch.optim.Adam(generator.parameters(), lr=opt.lr_gen, betas=(0, 0.99))
    optimizer_d = torch.optim.Adam(critic.parameters(), lr=opt.lr_disc, betas=(0, 0.99))
    loss_gen = losses.GeneratorLoss(opt)
    loss_critic = losses.CriticLossPatch(opt)
    logger = trainer.GANLogger(opt.run_name, checkpoint_frequency=opt.save_frequency,
                               log_tqdm=False, log_tensorboard=True)
    network_trainer = trainer.GANTrainer(generator, critic, data_generation,
                                         loss_gen, loss_critic,
                                         optimizer_g, optimizer_d,
                                         train_dataloader, logger=logger)

    if opt.restore:
        network_trainer.restore(os.path.join("models", opt.run_name), "complete")
    elif opt.restore_dir:
        network_trainer.restore(opt.restore_dir, "model_only")

    network_trainer.train(n_epochs=opt.n_epochs)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--run-name', default='tmp')
    parser.add_argument('--dataset', default='precomputed')
    parser.add_argument('--data-path', default='tmp')
    parser.add_argument('--bb-path', default='tmp')
    parser.add_argument('--part-path', default='tmp')
    parser.add_argument('--voxel-path', default='tmp')
    parser.add_argument('--n-epochs', type=int, default=500)
    parser.add_argument('--batch-size', type=int, default=32)
    parser.add_argument('--random-seed', type=int, default=42)
    parser.add_argument('--save-frequency', type=int, default=100)
    parser.add_argument('--parallel', action='store_true')
    parser.add_argument('--restore', action='store_true')
    parser.add_argument('--restore-dir', default='')
    parser.add_argument('--ae-path', default='')
    parser.add_argument('--ae-arch', default='high-res')
    parser.add_argument('--no-cuda', action='store_true')
    # gan parameter
    parser.add_argument('--mask', default='None')
    parser.add_argument('--generator-arch', default='convolutional')
    parser.add_argument('--bottleneck', type=int, default=128)
    parser.add_argument('--latent-dim', type=int, default=8)
    parser.add_argument('--soft-labels', action='store_true')
    parser.add_argument('--noisy-labels', action='store_true')
    parser.add_argument('--instance-noise', type=float, default=0.0)
    parser.add_argument('--gradient-penalty', type=float, default=0.0)
    parser.add_argument('--penalty-center', type=float, default=0.0)
    parser.add_argument('--penalty-reduction', default='max')
    parser.add_argument('--penalty-target', default='interpolation')
    parser.add_argument('--lr-gen', type=float, default=1e-3)
    parser.add_argument('--lr-disc', type=float, default=1e-3)
    parser.add_argument('--loss', default='NS')

    args = parser.parse_args()

    if args.restore:
        args = pickle.load(open(os.path.join("models", args.run_name, 'opt.pkl'), "rb"))
        args.restore = True
    util.set_random_seed(args.random_seed, False)
    args.device = torch.device('cpu') if args.no_cuda else torch.device('cuda')

    print(args)
    train(args)
