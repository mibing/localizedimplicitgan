import argparse
import json
import os
from glob import glob

import h5py
import numpy as np
import torch
import torch.nn.functional as fn
import torch.utils.data
import trimesh
from skimage import measure
from tqdm import tqdm

import datasets
import models
import preprocessing
import util


def get_child_ids(json_file, ids):
    ids.append(json_file['id'])
    if 'children' not in json_file:
        return
    for c in json_file['children']:
        get_child_ids(c, ids)


def precompute_aabb(opt):
    mesh_files = sorted(glob(opt.voxel_path + "/" + str(opt.class_label) + "/*"))

    if opt.mode == 'train':
        mesh_files = mesh_files[:int(0.8 * len(mesh_files))]
    elif opt.mode == 'test':
        mesh_files = mesh_files[int(0.8 * len(mesh_files)):]

    n_files = len(mesh_files)
    print(n_files)
    bb_path = opt.dataset_path + '/mask/shapenet_' + opt.mode + '_' + opt.class_label + '_aabb.hdf5'
    with h5py.File(bb_path, 'w') as file:
        aabb = file.create_dataset('aabb', (n_files, 3, 2), dtype=np.float32)

        for i, path in enumerate(tqdm(mesh_files)):
            grid, position, score = preprocessing.load_hsp(path, 256)
            bb = util.bb_from_voxel(grid)
            aabb[i, :, :] = bb


def precompute_part_aabb_chair(path, aabb):
    parts = ["chair_back", "chair_seat", "chair_base", "chair_head", "chair_arm", "footrest"]
    with open(path + '/result.json') as f:
        points = np.loadtxt(path + '/point_sample/pts-10000.pts')[:, :3]
        label = np.loadtxt(path + '/point_sample/label-10000.txt')
        util.transform_to_bb(points, aabb)

        json_file = json.load(f)
        complete_ids = [[]] * 6
        for child in json_file[0]['children']:
            try:
                new_id = parts.index(child["name"])
                ids = []
                get_child_ids(child, ids)
                complete_ids[new_id] = complete_ids[new_id] + ids
            except:
                print(path)

        bbs = torch.zeros([6, 3, 2])
        for i in range(6):
            if complete_ids[i]:
                indices = np.array(np.in1d(label, complete_ids[i]))
                part = points[indices]

                bb_max = part.max(0)
                bb_min = part.min(0)

                bbs[i, 0, 0] = bb_min[0]
                bbs[i, 0, 1] = bb_max[0]
                bbs[i, 1, 0] = bb_min[1]
                bbs[i, 1, 1] = bb_max[1]
                bbs[i, 2, 0] = bb_min[2]
                bbs[i, 2, 1] = bb_max[2]

    return bbs


def precompute_parts(opt):  # currently only implemented for chairs
    mesh_files = sorted(glob(opt.voxel_path + "/" + opt.class_label + "/*"))
    mesh_files = [file.split('/')[-1].split('.')[0] for file in mesh_files]
    if opt.mode == 'train':
        mesh_files = mesh_files[:int(0.8 * len(mesh_files))]
    elif opt.mode == 'test':
        mesh_files = mesh_files[int(0.8 * len(mesh_files)):]

    translate = dict()
    split_file = opt.partnet_path + '/metadata/stats/train_val_test_split/' + 'Chair.train.json'
    with open(split_file) as json_file:
        data = json.load(json_file)
        for entry in data:
            translate[entry['model_id']] = entry['anno_id']

    split_file = opt.partnet_path + '/metadata/stats/train_val_test_split/' + 'Chair.test.json'
    with open(split_file) as json_file:
        data = json.load(json_file)
        for entry in data:
            translate[entry['model_id']] = entry['anno_id']

    split_file = opt.partnet_path + '/metadata/stats/train_val_test_split/' + 'Chair.val.json'
    with open(split_file) as json_file:
        data = json.load(json_file)
        for entry in data:
            translate[entry['model_id']] = entry['anno_id']

    n_files = len(mesh_files)
    print(n_files)
    count = 0
    bb_path_parts = opt.dataset_path + '/mask/shapenet_' + opt.mode + '_' + opt.class_label + '_part_aabb.hdf5'
    bb_path = opt.dataset_path + '/mask/shapenet_' + opt.mode + '_' + opt.class_label + '_aabb.hdf5'
    with h5py.File(bb_path_parts, 'w') as master:
        aabb = master.create_dataset('aabb', (n_files, 6, 3, 2), dtype=np.float32)
        with h5py.File(bb_path, 'r') as f:
            for i, file in enumerate(mesh_files):
                if file in translate:
                    path = opt.partnet_path + '/data_v0/' + translate[file]
                    bbs = precompute_part_aabb_chair(path, f['aabb'][i])
                    count += 1
                else:
                    bbs = torch.zeros([6, 3, 2])
                aabb[i] = bbs
    print(count)


def truth_latent(opt):
    dataset = datasets.ShapeNetIMGAN(opt.voxel_path,
                                     opt.class_label,
                                     mode=opt.mode,
                                     resolution_input=opt.resolution)

    dataloader = torch.utils.data.DataLoader(dataset,
                                             batch_size=opt.batch_size,
                                             shuffle=False,
                                             drop_last=False,
                                             collate_fn=dataset.sample_objects,
                                             num_workers=8)

    class_names = sorted(glob(opt.voxel_path + '/*'))
    class_names = [name.split("/")[-1] for name in class_names]
    synset = sorted(set(class_names))
    synset_to_label = dict((s, label) for label, s in enumerate(synset))

    ae_path = opt.model_path + opt.ae_name + "/model_train.state"

    net = models.load_auto_encoder(opt.ae_arch, opt.latent_dim, ae_path, opt.interpolate,
                                   vae=False, grid=True).cuda().eval()

    identifier_old = "_" + opt.ae_name + "_" + opt.class_label + "_" + opt.mode
    latent_path = opt.dataset_path + '/latent/shapenet' + identifier_old

    n_files = 0
    for i, batch in enumerate(tqdm(dataloader)):
        grid = batch[2]
        path = batch[3]
        batch_size = grid.shape[0]

        if opt.mask_latent:
            bb_path = opt.dataset_path + '/mask/shapenet_' + opt.mode + '_' + opt.class_label + '_part_aabb.hdf5'
            with h5py.File(bb_path, "r") as f:
                bb = torch.from_numpy(f['aabb'][i * opt.batch_size: (i + 1) * opt.batch_size]).float()
                bb_mask = util.mask_from_bb_batch_parts(bb, opt.resolution).long()
                features = []
                for j in range(4):  # TODO
                    masked_grid = (grid - bb_mask[:, j]).cuda().float()
                    with torch.no_grad():
                        feature = net.encode(masked_grid)[0].cpu()
                        features.append(feature)
                feature = torch.stack(features, dim=1)
        else:
            grid = grid.cuda().float()
            with torch.no_grad():
                feature = net.encode(grid)[0].cpu()

        model_id = [path[i].split("/")[-1][:-4] for i in range(batch_size)]
        if opt.class_label == 'complete':
            class_id = [synset_to_label[path[i].split("/")[-2]] for i in range(batch_size)]
        else:
            class_id = None

        save_latent_to_file(latent_path + "_{}.hdf5".format(i), feature, model_id, class_id)
        n_files = i + 1

    identifier_new = "_" + opt.class_label + "_" + opt.mode + "_" + opt.ae_name
    if opt.mask_latent:
        identifier_new += "_masked"
    output_path = opt.dataset_path + '/latent/shapenet' + identifier_new + '.hdf5'

    n_shapes = 0
    for i in range(n_files):
        file = latent_path + "_{}.hdf5".format(i)
        with h5py.File(file, 'r') as f:
            pts = f['latent']
            n_shapes += pts.shape[0]
    print(n_shapes)

    with h5py.File(output_path, 'w') as master_file:
        if opt.mask_latent:  # TODO
            latent = master_file.create_dataset('latent', (n_shapes, 4, opt.latent_dim, 32, 32, 32), dtype=np.float32)
        else:
            latent = master_file.create_dataset('latent', (n_shapes, opt.latent_dim, 32, 32, 32), dtype=np.float32)
        string_type = h5py.special_dtype(vlen=str)
        model_ids = master_file.create_dataset('model_id', (n_shapes,), dtype=string_type)

        if opt.class_label == 'complete':
            class_ids = master_file.create_dataset('class_id', (n_shapes,), dtype=np.int)

        cur_idx = 0
        for i in range(n_files):
            # for file in tqdm(h5_files):
            file = latent_path + "_{}.hdf5".format(i)
            with h5py.File(file, 'r') as f:
                n_pts = f['latent'].shape[0]
                latent[cur_idx:cur_idx + n_pts] = f['latent'][:]
                model_ids[cur_idx:cur_idx + n_pts] = f['model_id'][:]
                if opt.class_label == 'complete':
                    class_ids[cur_idx:cur_idx + n_pts] = f['class_id'][:]
                cur_idx += n_pts


def gan_dataset(opt):
    ae_path = opt.model_path + opt.ae_name + "/model_train.state"
    gan_path = opt.model_path + opt.gan_name + '/epoch_' + str(opt.epoch) + '/generator.state'
    identifier = "_" + str(opt.resolution) + "_" + opt.ae_name + "_" + opt.gan_name + "_" + str(opt.epoch)
    output_path = opt.dataset_path + '/gan/shapenet' + identifier + '.hdf5'
    output_path_mesh = opt.dataset_path + '/gan_mesh/shapenet' + identifier
    points = []

    if opt.gan_mesh:
        os.makedirs(output_path_mesh)

    gan_model = models.load_generator(opt.gan_arch, opt.latent_dim, 128, opt.mask_channel,
                                      gan_path, opt.parallel).cuda().eval()
    classifier_model = models.load_auto_encoder(opt.ae_arch, opt.latent_dim, ae_path, opt.interpolate,
                                                vae=False, grid=True).cuda().eval()

    if opt.conditional:
        bb_path = opt.dataset_path + '/mask/shapenet_test_' + opt.class_label + '_part_aabb.hdf5'
        with h5py.File(bb_path, "r") as f:
            bbs = torch.from_numpy(f['aabb'][:])
            n_files = bbs.shape[0]
        for i in tqdm(range(opt.n_objects)):
            mask = util.mask_from_bb_batch(bbs[i % n_files].unsqueeze(0), opt.mask_res).long().cuda()
            mask = fn.one_hot(mask).permute(0, 4, 1, 2, 3).float()
            points.append(util.normalize_unit_sphere(
                sample_implicit_function(gan_model, classifier_model, mask, opt.resolution, opt.n_points,
                                         output_path_mesh, i, opt.gan_points, opt.gan_mesh).transpose(1, 0)))
    else:
        for i in tqdm(range(opt.n_objects)):
            points.append(util.normalize_unit_sphere(
                sample_implicit_function(gan_model, classifier_model, None, opt.resolution, opt.n_points,
                                         output_path_mesh, i, opt.gan_points, opt.gan_mesh).transpose(1, 0)))

    if opt.gan_points:
        with h5py.File(output_path, 'w') as master_file:
            out_points = master_file.create_dataset('samples', (opt.n_objects, 3, opt.n_points), dtype=np.float32)
            out_points[:] = torch.stack(points, dim=0)


def save_latent_to_file(out_file, batch_latent, batch_model_ids, batch_class_ids=None):
    with h5py.File(out_file, 'a') as f:
        latent = f.create_dataset('latent', batch_latent.shape, dtype=np.float32)
        latent[:] = batch_latent

        string_type = h5py.special_dtype(vlen=str)
        model_ids = f.create_dataset('model_id', (batch_latent.shape[0],), dtype=string_type)
        model_ids[:] = batch_model_ids

        if batch_class_ids is not None:
            class_ids = f.create_dataset('class_id', (batch_latent.shape[0],), dtype=int)
            class_ids[:] = batch_class_ids

        f.close()


def save_grid_to_file(out_file, batch_grid, batch_model_ids, batch_class_ids=None):
    with h5py.File(out_file, 'a') as f:
        grid = f.create_dataset('grid', batch_grid.shape, dtype=np.bool)
        grid[:] = batch_grid

        string_type = h5py.special_dtype(vlen=str)
        model_ids = f.create_dataset('model_id', (batch_grid.shape[0],), dtype=string_type)
        model_ids[:] = batch_model_ids

        if batch_class_ids is not None:
            class_ids = f.create_dataset('class_id', (batch_grid.shape[0],), dtype=int)
            class_ids[:] = batch_class_ids

        f.close()


def sample_voxel(voxel, n_points=2500, path=None, idx=0, sample_points=True, save_mesh=False):
    verts, faces, normals, values = measure.marching_cubes_lewiner(voxel, 0.5)

    mesh = trimesh.Trimesh(vertices=verts / voxel.shape[-1] - 0.5,
                           faces=faces)

    if save_mesh:
        filename = path + "/" + str(idx) + ".obj"
        mesh.export(filename)

    if sample_points:
        points = mesh.sample(n_points)
    else:
        points = torch.randn((3, n_points)).numpy()

    return torch.from_numpy(points).float()


def sample_implicit_function(gan, classifier, mask=None, grid_size=256,
                             n_points=2500, path=None, idx=0, sample_points=True, save_mesh=False):
    with torch.no_grad():
        success = False
        while not success:
            noise = torch.randn((1, 128), device=torch.device('cuda'))
            grid = util.meshgrid(grid_size, device=torch.device('cuda')).view(1, 3, -1)
            features = gan(noise, mask)
            voxel = classifier.decode(features, grid).view(grid_size, grid_size, grid_size).cpu().numpy()
            if voxel.min() < 0.5 < voxel.max():
                success = True

    points = sample_voxel(voxel, n_points, path, idx, sample_points, save_mesh)

    return points


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # task
    parser.add_argument('--truth-latent', action='store_true')
    parser.add_argument('--mask-latent', action='store_true')
    parser.add_argument('--gan-points', action='store_true')
    parser.add_argument('--gan-mesh', action='store_true')
    parser.add_argument('--gan-latent', action='store_true')
    parser.add_argument('--bounding-box', action='store_true')
    parser.add_argument('--bounding-box-partial', action='store_true')
    # ae params
    parser.add_argument('--ae-name', default='tmp')
    parser.add_argument('--ae-arch', default='high-res')
    parser.add_argument('--interpolate', action='store_true')
    parser.add_argument('--latent-dim', type=int, default=32)
    # gan params
    parser.add_argument('--gan-name', default='tmp')
    parser.add_argument('--gan-arch', default='AdaIN')
    parser.add_argument('--parallel', action='store_true')
    parser.add_argument('--conditional', action='store_true')
    parser.add_argument('--mask-res', type=int, default=8)
    parser.add_argument('--mask-channel', type=int, default=0)
    parser.add_argument('--epoch', type=int, default=500)
    # data params
    parser.add_argument('--mode', default='train')
    parser.add_argument('--batch-size', type=int, default=4)
    parser.add_argument('--n-points', type=int, default=2048)
    parser.add_argument('--n-objects', type=int, default=2048)
    parser.add_argument('--class-label', default='03001627')
    parser.add_argument('--resolution', type=int, default=256)
    parser.add_argument('--voxel-path', default='tmp')
    parser.add_argument('--partnet-path', default='tmp')
    parser.add_argument('--dataset-path', default='tmp')
    parser.add_argument('--model-path', default='tmp')
    args = parser.parse_args()

    if args.truth_latent or args.mask_latent:
        truth_latent(args)
    elif args.gan_mesh or args.gan_points:
        gan_dataset(args)
    elif args.bounding_box:
        precompute_aabb(args)
    elif args.bounding_box_partial:
        precompute_parts(args)
    else:
        print("ERROR: no task given")
